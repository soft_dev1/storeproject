/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantida.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;
import model.User;

/**
 *
 * @author Admin
 */
public class TestUpdateUser {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = database.Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "UPDATE user SET  name = ?,tel = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
           User user = new User(2, "Mana", "0844214141");
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getTel());
            stmt.setInt(3, 6);
            int row = stmt.executeUpdate();
            System.out.println("Affect Row" + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
