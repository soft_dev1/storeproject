/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantida.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;
import model.User;

/**
 *
 * @author Admin
 */
public class TestInsertUser {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = database.Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "INSERT INTO user (name,tel) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            User user = new User(-1, "Maya","0841561145");
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getTel());
            int row = stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            int id = -1;

            if (rs.next()) {
                id = rs.getInt(1);
            }
            System.out.println("Affect Row" + row + "id" + id);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}

