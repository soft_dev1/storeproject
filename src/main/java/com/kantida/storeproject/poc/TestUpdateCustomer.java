/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantida.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.User;

/**
 *
 * @author Admin
 */
public class TestUpdateCustomer {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = database.Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "UPDATE customer SET  name = ?,tel = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Customer cus = new Customer(-1, "Mint","0855412147");
            stmt.setString(1, cus.getName());
            stmt.setString(2, cus.getTel());
            stmt.setInt(3, 6);
            int row = stmt.executeUpdate();
            System.out.println("Affect Row" + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
