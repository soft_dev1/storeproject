/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantida.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class TestDeleteUser {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = database.Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "DELETE FROM user WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 1);
            int row = stmt.executeUpdate();
            System.out.println("Affect Row" + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
